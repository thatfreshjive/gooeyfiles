package browser;

//import browser.FileTableModel.*;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Container;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.*;
import java.awt.image.*;

import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;
import javax.swing.tree.*;
import javax.swing.table.*;
import javax.swing.filechooser.FileSystemView;

import javax.imageio.ImageIO;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;

import java.io.*;
import java.nio.channels.FileChannel;

import java.net.URL;

class InitialBrowser {

	// Constants
	private final int WINDOW_HEIGHT = 800;
	private final int WINDOW_WIDTH = 800;
	private final int[] COL_WIDTHS = {
		(int) 0.2 * WINDOW_WIDTH,
		(int) 0.35 * WINDOW_WIDTH,
		(int) 0.15 * WINDOW_WIDTH,
		(int) 0.15 * WINDOW_WIDTH,
		(int) 0.05 * WINDOW_WIDTH,
		(int) 0.05 * WINDOW_WIDTH,
		(int) 0.05 * WINDOW_WIDTH,
		(int) 0.05 * WINDOW_WIDTH,
		(int) 0.05 * WINDOW_WIDTH,
	}

	// Main GUI Container
	private JPanel gui;

	// Directory Listing
	private JTable table;

	// Table model for file
	private FileTableModel fileTableModel;
	private ListSelectionListener listSelectionListener;
	private boolean cellSizesSet = false;
	private int rowHeight = 25;

	/* File details. */
	private JLabel fileName;
	private JTextField path;
	private JLabel date;
	private JLabel size;
	private JCheckBox readable;
	private JCheckBox writable;
	private JCheckBox executable;
	private JRadioButton isDirectory;
	private JRadioButton isFile;

	public Container getGui() {
		if (gui == null) {
			gui = new JPanel(new BorderLayout(3, 3));
			gui.setBorder(new EmptyBorder(5, 5, 5, 5));

			table = new JTable();
			//table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			table.setAutoCreateRowSorter(true);
			table.setPreferredScrollableViewportSize(new Dimension(500,500));
			table.setFillsViewportHeight(true);
			table.setShowVerticalLines(false);

            table.addMouseListener(new MouseAdapter()){
                @Override
                public void mouseClicked(MouseEvent evt){
                    int row = table.rowAtPoint(evt.gePoint());
                    int col = table.columnAtPoint(evt.getPoint());

                }
            }

			//table.getSelectionModel().addListSelectionListener(listSelectionListener);
			JScrollPane tableScroll = new JScrollPane(table);
			gui.add(tableScroll);
			//Dimension d = tableScroll.getPreferredSize();
			//tableScroll.setPreferredSize(new Dimension((int) d.getWidth(), (int) d.getHeight() / 2));
		}
		return gui;
	}

	/**
	 * Update the table on the EDT
	 */
	private void setTableData(final File[] files) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				if (fileTableModel == null) {
					fileTableModel = new FileTableModel() {};
					table.setModel(fileTableModel);
				}
				table.getSelectionModel().removeListSelectionListener(listSelectionListener);
				fileTableModel.setFiles(files);
				table.getSelectionModel().addListSelectionListener(listSelectionListener);
				if (!cellSizesSet) {
					table.setRowHeight(rowHeight);
					for(int i=0; i<FileTableModel.getColumnCount; i++) {
						setColumnWidth(i, COL_WIDTHS[i]);
					}
//					setColumnWidth(0, 0.4*WINDOW_WIDTH);
//					setColumnWidth(1, 0.5*WINDOW_WIDTH);
//					setColumnWidth(2, -1);
//					setColumnWidth(3, -1);
//					setColumnWidth(4, -1);
//					setColumnWidth(5, -1);
//					setColumnWidth(6, -1);
//
					cellSizesSet = true;
				}
			}
		});

	}

	private void setColumnWidth(int column, int width) {
		TableColumn tableColumn = table.getColumnModel().getColumn(column);
		if (width < 0) {
			// use the preferred width of the header..
			JLabel label = new JLabel((String) tableColumn.getHeaderValue());
			Dimension preferred = label.getPreferredSize();
			// altered 10->14 as per camickr comment.
			width = (int) preferred.getWidth() + 15;
		}
		tableColumn.setPreferredWidth(width);
		tableColumn.setMaxWidth(width);
		tableColumn.setMinWidth(width);
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				try {
					// Significantly improves the look of the output in
					// terms of the file names returned by FileSystemView!
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (Exception e) {
				}
				JFrame f = new JFrame("Gooey Files");
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				InitialBrowser FileBrowser = new InitialBrowser();
				f.setContentPane(FileBrowser.getGui());

				f.pack();
				f.setLocationByPlatform(true);
				f.setMinimumSize(f.getSize());
				f.setVisible(true);

				File init = new File("C:");

				FileBrowser.setTableData(init.listFiles());
			}
		});
	}
}
