/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package browser;

import java.io.File;
import java.util.Date;
import javax.swing.table.AbstractTableModel;
import java.util.Random;

/**
 *
 * @author TJ
 */
public abstract class FileTableModel extends AbstractTableModel {

		private File[] files;
		private final String[] columns = {
				"Name", "Path", "Size", "Last Modified",
				"R", "W", "E", "D", "F",};

		public FileTableModel() {
				this(new File[0]);
		}

		public FileTableModel(File[] files) {
				this.files = files;
		}

		@Override
		public Object getValueAt(int row, int column) {
				File file = files[row];
				switch (column) {
						case 0:
								String[] splitPath = file.getName().split("[\\/]");
								return splitPath[splitPath.length - 1];
						case 1:
								return file.getPath();
						case 2:
								return file.length();
						case 3:
								return file.lastModified();
						case 4:
								return file.canRead();
						case 5:
								return file.canWrite();
						case 6:
								return file.canExecute();
						case 7:
								return file.isDirectory();
						case 8:
								return file.isFile();
						default:
								System.err.println("Logic Error");
				}
				return "";
		}

		@Override
		public Class<?> getColumnClass(int column) {
				if (column == 2) {
						return Long.class;
				} else if (column == 3) {
						return Date.class;
				} else if (column == 8) {
						return Boolean.class;
				} else {
						return String.class;
				}
		}

		@Override
		public String getColumnName(int column) {
				return columns[column];
		}

		public int getColumnCount() {
				return columns.length;
		}

		@Override
		public int getRowCount() {
				return files.length;
		}

		public File getFile(int row) {
				return files[row];
		}

		public void setFiles(File[] files) {
				this.files = files;
				fireTableDataChanged();
		}

        public String getNameAtIndex(int index) {
            String path = this.files[index];
            String[] splitPath = path.split("[\\/]");
            return splitPath[splitPath.length - 1];

        }

        public void shuffleFiles() {
            int index;
            File temp = new File();
            Random rnd = new Random();
            for(int i = getRowCount()-1; i > 0; i--){
                index = rnd.nextInt(i + 1);
                temp = this.files[index];
                this.files[index] = this.files[i];
                this.files[i] = temp;
            }
            fireTableDataChanged();
        }

        public void insertionSortFiles() {
            for(int i = 1; i < getRowCount(); i++){
                for(int j = i; j > 0 && getNameAtIndex(j).compareTo(getNameAtIndex[j+1]); j--){
                    File temp = this.files[j];
                    this.files[j+1] = this.files[j];
                    this.files[j] = temp;
                }
                fireTableDataChanged();
            }
        }
}
